package org.example.customer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CustomerTest {

    @Test
    void itShouldMapDtoIntoCustomer() {
        // Arrange
        CustomerDTO customerDTO = new CustomerDTO("John", "Wick", "L.A", "USA");
        // Act
        Customer customer = Customer.from(customerDTO);
        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(customerDTO.firstName(), customer.getFirstName()),
                () -> Assertions.assertEquals(customerDTO.lastName(), customer.getLastName())
        );
    }
}