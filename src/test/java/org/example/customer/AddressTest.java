package org.example.customer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AddressTest {

    @Test
    void itShouldMapDtoIntoAddress() {
        // Arrange
        CustomerDTO customerDTO = new CustomerDTO("Jammal", "Tiger", "L.A", "USA");
        var customerId = 1;
        // Act
        Address address = Address.from(customerId, customerDTO);
        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(customerId, address.getCustomerId() ),
                () -> Assertions.assertEquals(customerDTO.city(), address.getCity() ),
                () -> Assertions.assertEquals(customerDTO.country(), address.getCountry() )
        );
    }
}