package org.example.customer;

public record CustomerDTO(String firstName, String lastName, String city, String country) {
}
