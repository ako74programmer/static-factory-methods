package org.example.customer;

public class Customer {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String lastName;

    private Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public static Customer from(CustomerDTO customerDTO) {
        return new Customer(customerDTO.firstName(), customerDTO.lastName());
    }
}
