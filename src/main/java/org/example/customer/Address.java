package org.example.customer;

public class Address {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer customerId;
    private String city;
    private String country;

    private Address(Integer customerId, String city, String country) {
        this.customerId = customerId;
        this.city = city;
        this.country = country;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public static Address from(Integer customerId, CustomerDTO customerDTO) {
        return new Address(customerId, customerDTO.city(), customerDTO.country());
    }
}
